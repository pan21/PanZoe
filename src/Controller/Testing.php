<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/9
 * Time: 15:38
 */

namespace PanZoe\Pan\Controller;
use App\Http\Controllers\Controller;
use PanZoe\Pan\Facades\Pan;

class Testing extends Controller
{

	public function index()
	{
		return Pan::test();
	}
}