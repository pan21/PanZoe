<?php

namespace PanZoe\Pan;
use Illuminate\Support\ServiceProvider;

class PanZoeServiceProvider extends ServiceProvider
{
//	protected $defer = true; // 延迟加载服务
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
		$this->loadViewsFrom(__DIR__ . '/views', 'PanZoe'); // 视图目录指定
		$this->loadRoutesFrom(__DIR__.'/routes.php');
		$this->publishes([
			__DIR__.'/config/pan.php' => config_path('pan.php'),
			__DIR__.'/views' => resource_path('views/vendor/PanZoe'),
		]);



    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
		$this->app->singleton('Pan', function ($app) {
			return new Testing($app);
		});
    }
}
