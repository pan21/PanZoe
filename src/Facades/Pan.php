<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/9
 * Time: 15:00
 */

namespace PanZoe\Pan\Facades;


use Illuminate\Support\Facades\Facade;

class Pan extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'Pan';
	}

}